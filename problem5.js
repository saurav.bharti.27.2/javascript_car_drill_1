const p4 = require('./problem4')



function problem5(inventory){
    try{
        const p5_arr = p4(inventory)
        if(!p5_arr){
            throw new Error("inventory is invalid")
        }
        const older_cars = []
        for(let index=0;index<p5_arr.length; index++){
            if(p5_arr[index]< 2000 ){
                older_cars.push(p5_arr[index]);
            }
        }
    
        if(older_cars)
            console.log(older_cars.length, " <- number of cars older than 2000")
        return older_cars;

    }catch(err){
        console.log(err.stack)
    }


}


module.exports= problem5