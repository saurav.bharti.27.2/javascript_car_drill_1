const problem5 = require('../problem5')

const inventory = require('../inventory')

try{
    if(!inventory)
        throw new Error("Inventory is not present")
    const result = problem5(inventory)
}catch(err){
    console.log(err.stack)
}