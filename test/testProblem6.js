const problem6 = require('../problem6')
const inventory  = require('../inventory')



try{
    if(!inventory)
        throw new Error("Inventory is not present")
    const result = problem6(inventory)
}catch(err){
    console.log(err.stack)
}