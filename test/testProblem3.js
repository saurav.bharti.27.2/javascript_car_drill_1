const inventory = require('../inventory')
const problem3= require('../problem3')  

try{
    if(!inventory){
        throw new Error("Inventory is empty.")
    }

    const result = problem3(inventory)
}catch(err){
    console.log(err.stack)
}
