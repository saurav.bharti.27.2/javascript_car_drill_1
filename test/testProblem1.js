const problem1= require('../problem1')
const inventory= require('../inventory')

try{
    if(!inventory){
        throw new Error("Inventory is empty.")
    }
    // (car_id, Inventory)
    const result = problem1(33, inventory)
    console.log(result)
}catch(err){
    console.log(err.stack)
}
