const problem4 = require('../problem4')
const inventory = require('../inventory')

try{
    if(!inventory)
        throw new Error("Inventory is not present")
    const result = problem4(inventory)
    
}catch(err){
    console.log(err.stack)
}