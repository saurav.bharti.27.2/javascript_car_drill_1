

function problem1(car_id, inventory){
    if(!car_id || (typeof car_id)!=='number'){
        return "Car_id is invalid."
    }

    
    for(let index=0;index<inventory.length;index++){
        if( "id" in inventory[index] && inventory[index].id=== car_id){
            return `Car ${car_id} is a ${("car_year" in inventory[index] )? inventory[index].car_year : ''}, ${("car_make" in inventory[index])? inventory[index].car_make: ''}, ${("car_model" in inventory[index]) ? inventory[index].car_model : ''}`
        }
    }

    return `Car which has ${car_id}, is not found.`
}


module.exports = problem1