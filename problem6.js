
function problem6(inventory){


    const car_picked=[]
    for(let index=0;index<inventory.length;index++){

        if("car_make" in inventory[index] && (inventory[index].car_make=="BMW" || inventory[index].car_make=="Audi")){
            car_picked.push(inventory[index])
        }
    }
    if(car_picked.length==0){
        return "None of the car found of BMW and Audi type"
    }

    console.log(JSON.stringify(car_picked))
}

module.exports = problem6