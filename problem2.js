
function problem2(inventory){
    let last = inventory.length - 1;

    if(!("car_make" in inventory[last]) && !("car_make" in inventory[last])){
        return "Last car doesn't have car make and car model type"
    }
    else if(!("car_make" in inventory[last])){
        return "Last car doesn't have car make type"
    }
    else if(!("car_model" in inventory[last])){
        return "Last car doesn't have car model"
    }


    return `Last car is a ${inventory[last].car_make}, ${inventory[last].car_model}`
}

module.exports = problem2
